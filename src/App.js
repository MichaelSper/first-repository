import './App.css';
import React ,{useState,useEffect} from 'react'
import Form from './components/Form'
import TodoList from './components/TodoList';

function App() {

  const [inputText, setInputText] = useState("");
  const [todos, setTodos] = useState([]);
  const [status, setStatus] = useState('all');
  const [filteredTodos, setFilteredTodos] = useState([]);


  //Functions
  const filterHandler = () => {
    switch (status) {
      case 'completed':
        setFilteredTodos(todos.filter(todo => todo.completed === true));
        break;
        case 'uncompleted':
          setFilteredTodos(todos.filter(todo => todo.completed === false));
        break;
      default:
        setFilteredTodos(todos);
        break;
    }
  }
  const saveLlocalTodos = () => {
      localStorage.setItem("todos", JSON.stringify(todos));
  }
  const getLlocalTodos = () => {

  }
  //Use effect
  useEffect(() => {
    getLlocalTodos
  }, []);
  useEffect(() => {
    filterHandler();
    saveLlocalTodos();
  }, [todos,status]);
  return (
    <div className="App">
      <header>
      <h1>Michael's Todo List</h1>
      </header>
      <Form todos={todos} setTodos={setTodos} setInputText={setInputText} inputText={inputText}
      setStatus={setStatus}
      filteredTodos ={filteredTodos}
      />
      <TodoList setTodos={setTodos}
      filteredTodos ={filteredTodos}

        todos={todos} />
    </div>
  );
}

export default App;
